const {By} = require("selenium-webdriver/lib/by");
const {until} = require("selenium-webdriver");

module.exports.StartPage = class StartPage {


    constructor(driver) {
        this.driver = driver

    }

    async startClick() {
        let buttonRun = await this.driver.findElement(By.xpath('//*[@id="promptToRun"]/div/button'));
        await this.driver.wait(until.elementIsVisible(buttonRun), 2000);
        await buttonRun.click()
    }

    async addButtonClick() {
        let buttonAdd = await this.driver.findElement(By.css('.btn.btn-primary'));
        await this.driver.wait(until.elementIsVisible(buttonAdd), 2000);
        await buttonAdd.click();
    }

    async addCertificate(list) { //??????????????????????????????????/
        await this.driver.wait(until.elementsLocated(By.css('dropbox.dropbox')), 2000);
        let area = await this.driver.findElement(By.css('dropbox.dropbox'));
        // const actions = this.driver.actions({async: true});
        // await actions.dragAndDrop(area, "sdsdssdsdsdsdsd").perform();
        await area.sendKeys("sdsdsdsdsd");
        await area.submit();

    };

    async getDisplayedData(stu) {
        let listBoxNames = []
        let listMap = []
        let result = [listBoxNames, listMap]
        await this.driver.wait(until.elementsLocated(By.css("div.list-group.mb-3 a")), 2000);
        let listElements = await this.driver.findElements(By.css("div.list-group.mb-3 a"))
        for (let i = 0; i < listElements.length; i++) {
            let itemName = await listElements[i].getText()
            listBoxNames.push(itemName)
            let mapRow = new Map();
            await listElements[i].click()
            await this.driver.wait(until.elementsLocated(By.css("tbody")), 1000);
            let rows = await this.driver.findElements(By.css("tbody tr"))
            for (let j = 0; j < rows.length; j++) {
                let key = await rows[j].findElement(By.css("th")).getText()
                let value = await rows[j].findElement(By.css("td")).getText()
                mapRow.set(key, value)
            }
            listMap.push(mapRow)

        }

        return result
    }


}