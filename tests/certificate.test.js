const assert = require("assert");
const {Builder, Browser} = require("selenium-webdriver");
const {By} = require("selenium-webdriver/lib/by");
const {Certificate} = require("./model/certificate");
const {StartPage} = require("./page_objects/StartPage");
const fs = require("node:fs/promises");


describe('Element Information Test', function () {
    let cert1Body = "";
    let cert2Body = "";
    let invalidCertBody = "";
    let driver;
    let startPage;
    before(async function () {
        // driver = await new Builder().forBrowser('chrome').build();
        cert1Body = await getCertificate("./model/cert1.cer");
        cert2Body = await getCertificate("./model/cert2.cer");
        invalidCertBody = await getCertificate("./model/invalid.cer");
        driver = await new Builder().forBrowser(Browser.CHROME).build();
        await driver.get('https://js-55fbfg.stackblitz.io/');
        await driver.manage().setTimeouts({implicit: 3000});
        startPage = new StartPage(driver)
    });
    beforeEach(async () => {
        await driver.get('https://js-55fbfg.stackblitz.io/');
    })

    it('1.1 User should add one valid certificate and rerun page', async () => {
        let expectedCertificates = []
        await startPage.startClick()
        await startPage.addButtonClick();
        let cert1 = new Certificate(cert1Body)
        expectedCertificates.push(cert1)
        await startPage.addCertificate(expectedCertificates)
        let actualDisplayedData = await startPage.getDisplayedData()
        assert.equal(true, await validateLocalStorage(expectedCertificates, driver))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))
        driver.navigate().refresh();
        assert.equal(true, await validateLocalStorage(expectedCertificates, driver))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))

    })

    it('1.2 User should add several time one certificate and rerun page', async () => {
        let expectedCertificates = []
        await startPage.startClick()
        await startPage.addButtonClick();
        let cert1 = new Certificate(cert2Body)
        expectedCertificates.push(cert1)
        expectedCertificates.push(cert1)
        await startPage.addCertificate(expectedCertificates)
        let actualDisplayedData = await startPage.getDisplayedData()
        assert.equal(true, await validateLocalStorage(expectedCertificates))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))
        driver.navigate().refresh();
        assert.equal(true, await validateLocalStorage(expectedCertificates))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))

    })

    it('1.3 User should add several valid certificates and rerun page', async () => {
        let expectedCertificates = []
        await startPage.startClick()
        await startPage.addButtonClick();
        let cert1 = new Certificate(cert1Body)
        let cert2 = new Certificate(cert2Body)
        expectedCertificates.push(cert1)
        expectedCertificates.push(cert2)
        await startPage.addCertificate(expectedCertificates)
        let actualDisplayedData = await startPage.getDisplayedData()
        assert.equal(true, await validateLocalStorage(expectedCertificates))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))
        driver.navigate().refresh();
        assert.equal(true, await validateLocalStorage(expectedCertificates))
        assert.equal(true, validateDisplayedData(expectedCertificates, actualDisplayedData))

    })

    it('1.4 User should  add invalid  certificate  and rerun page', async () => {
        let listCertificates = []
        await startPage.startClick()
        await startPage.addButtonClick();
        let cert1 = new Certificate(invalidCertBody)
        listCertificates.push(cert1)
        await startPage.addCertificate(listCertificates)
        let alert = await driver.switchTo().alert();
        let alertText = await alert.getText();
        await alert.accept();
        let actualDisplayedData = await startPage.getDisplayedData()
        assert.equal("Ошибка при разборе сертификата", alertText)
        assert.equal(true, await validateLocalStorage([]))
        assert.equal(true, validateDisplayedData([], actualDisplayedData))
        driver.navigate().refresh();
        assert.equal(true, await validateLocalStorage([]))
        assert.equal(true, validateDisplayedData([], actualDisplayedData))

    })

    afterEach(async () => {
        await clearLocalStorage()
        driver.navigate().refresh();
    })

    after(async () => await driver.quit());
})

function validateDisplayedData(expectedCertificates, actualDisplayedData) {
    let result = false
    for (let i = 0; i < actualDisplayedData.length; i++) {
        let recordMap = actualDisplayedData[1][i]
        let keys = recordMap.keys()
        let item = actualDisplayedData[0][i].trim()
        assert.equal(item, recordMap.get(keys[0]), " Problem with Name in Listbox and Common Name value !")
        assert.equal("Common Name:", keys[0], "Problem with naming field Common Name")
        assert.equal(expectedCertificates[i].commonName, recordMap.get(keys[0]), "Problem with Common Name value in certificate and value displayed ")
        assert.equal("Issuer CN:", keys[1], "Problem with naming field Issuer CN")
        assert.equal(expectedCertificates[i].issuerCN, recordMap.get(keys[1]), "Problem with Issuer CN value in certificate and value displayed ")
        assert.equal("Valid From:", keys[2], "Problem with naming field Valid From")
        assert.equal(expectedCertificates[i].validFrom, recordMap.get(keys[2]), "Problem with Valid From value in certificate and value displayed ")
        assert.equal("Valid To:", keys[3], "Problem with naming field Valid To")
        assert.equal(expectedCertificates[i].validTo, recordMap.get(keys[3]), "Problem with Valid To value in certificate and value displayed ")

    }
    result = true
    return result;
}

async function clearLocalStorage() {
    await driver.executeScript((function () {
        return localStorage.clear();
    }),);
}

async function validateLocalStorage(expectedCertificates, driver) {
    let listFromStorage = await driver.executeScript((function () {
        return localStorage.get("certs");
    }),);
    assert.equal(expectedCertificates.length, listFromStorage.length, "Problem with number records in LocalStorage ")
    for (let i = 0; i < expectedCertificates; i++) {
        assert.equal(expectedCertificates[i], listFromStorage[i], "Problem with value certificates attribute in LocalStorage  and source certificates ")
    }
    return true;
}

async function getCertificate(path) {
    try {
        const data = await fs.readFile(path, {encoding: 'utf8'});
        return data;
    } catch (err) {
        console.log(err);
    }
}




