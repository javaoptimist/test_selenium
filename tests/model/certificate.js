const
    {ASN1} = require('@lapo/asn1js'),
    {Hex} = require('@lapo/asn1js/hex.js');



module.exports.Certificate = class Certificate {
    constructor(body) {
        this.isValid = false
        this.commonName = ""
        this.issuerCN = ""
        this.validFrom = ""
        this.validTo = ""
        this.body = body
        this.parse(body)


    }

    parse(body) {
        // ???????????????????????????????
    }

    equals(certificate) {
        return (this.commonName + this.issuerCN + this.validFrom + this.validTo) === (
            certificate.commonName + certificate.issuerCN + certificate.validFrom + certificate.validTo);
    }


}